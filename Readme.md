Depends on [pySerial](https://pythonhosted.org/pyserial/).

```
usage: reader.py [-h] [--serial_device [SERIAL_DEVICE]]
                 [--output_file [OUTPUT_FILE]] [--frequency [FREQUENCY]]
                 [--serial_timeout [SERIAL_TIMEOUT]]
                 [--minimum_amps [MINIMUM_AMPS]]
                 [--summarize_file [SUMMARIZE_FILE]]

Process some integers.

optional arguments:
  -h, --help            show this help message and exit
  --serial_device [SERIAL_DEVICE]
                        serial device
  --output_file [OUTPUT_FILE]
                        optional output file
  --frequency [FREQUENCY]
                        frequency (in Hz) to try to record at
  --serial_timeout [SERIAL_TIMEOUT]
                        read timeout for serial device
  --minimum_amps [MINIMUM_AMPS]
                        minimum amps to start/stop reading
  --summarize_file [SUMMARIZE_FILE]
                        file to summarize
```

The output file format is very simple. It's just rows of 6 binary floats in the following order, repeated over and over:

- time 
- voltage 
- current 
- power 
- voltage_data_pos 
- voltage_data_neg

Use Python's struct.unpack to parse it.
