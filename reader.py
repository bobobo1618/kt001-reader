#!/usr/bin/env python3

import argparse
import collections
import serial
import struct
import sys
import time

_HANDSHAKE_REQ = b'This is control'
_HANDSHAKE_RES = b'Roger\x00'
_GET_DATA = b'Get Meter Data'

Record = collections.namedtuple(
    'Record', [
        'time', 
        'voltage', 
        'current', 
        'power', 
        'voltage_data_pos', 
        'voltage_data_neg'
    ]
)

def record_serial(serial_device, frequency=1, serial_timeout=0.03, minimum_amps=0.01):
    ser = serial.Serial(serial_device, timeout=serial_timeout)
    ser.write(_HANDSHAKE_REQ)
    assert ser.read(len(_HANDSHAKE_RES)) == _HANDSHAKE_RES
    start_time = None
    while True:
        ser.write(_GET_DATA)
        data = ser.read(20)
        voltage, current, power, voltage_dp, voltage_dm = struct.unpack('5f', data)
        if not start_time:
            if current < minimum_amps:
                continue
            print("Starting to record")
            start_time = time.time()

        delta_time = time.time() - start_time
        yield Record(delta_time, voltage, current, power, voltage_dp, voltage_dm)
        if current < minimum_amps:
            print("Stopped recording after {}s".format(delta_time))
            return
        time.sleep(1.0/frequency)

def read_file_records(filename):
    with open(filename, 'rb') as input_file:
        while True:
            data = input_file.read(24)
            if not data:
                return
            record = Record(*struct.unpack('6f', data))
            yield record

def print_records(record_iterator):
    for record in record_iterator:
        print(repr(record))

def write_records(record_iterator, output_filename):
    with open(output_filename, 'wb') as output_file:
        for record in record_iterator:
            output_file.write(struct.pack('6f', *record))

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Process some integers.')
    argparser.add_argument('--serial_device', type=str, nargs='?', help='serial device', default='')
    argparser.add_argument('--output_file', type=str, nargs='?', help='optional output file', default='')
    argparser.add_argument('--frequency', type=float, nargs='?', help='frequency (in Hz) to try to record at', default=1)
    argparser.add_argument('--serial_timeout', type=float, nargs='?', help='read timeout for serial device', default=0.03)
    argparser.add_argument('--minimum_amps', type=float, nargs='?', help='minimum amps to start/stop reading', default=0.01)
    argparser.add_argument('--summarize_file', type=str, nargs='?', help='file to summarize', default='')
    args = argparser.parse_args()

    if args.serial_device:
        iterator = record_serial(args.serial_device, frequency=args.frequency, serial_timeout=args.serial_timeout, minimum_amps=args.minimum_amps)
        if args.output_file:
            write_records(iterator, args.output_file)
        else:
            print_records(iterator)
        sys.exit(0)
    
    if args.summarize_file:
        print_records(read_file_records(args.summarize_file))
        sys.exit(0)
    
    print("No actions specified.")
    argparser.print_help()
    sys.exit(1)
    
